const fillAlphabetHashMap = (line, lineNumber, alphabetHashmap) => {
  const separator = ' ';
  // console.log('@!@! fillAlphabetHashMap, line', line);
  line.split(separator).forEach((chr, charPosition) => {
    alphabetHashmap[chr] = alphabetHashmap[chr] || [];
    alphabetHashmap[chr].push([lineNumber, charPosition + 1]);
  });

  // console.log('@!@! fillAlphabetHashMap, alphabetHashmap', alphabetHashmap);
  return { ...alphabetHashmap };
};

module.exports = fillAlphabetHashMap;
