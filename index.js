const lineReader = require('line-reader');
const fs = require('fs');
const parseGridsize = require('./parseGridSize');
const fillAlphabetHashMap = require('./fillAlphabetHashMap');
const checkNextLetter = require('./ checkNextLetter');

const logger = fs.createWriteStream('log.txt', {
  flags: 'a', // 'a' means appending (old data will be preserved)
});

const myFile = 'alphabet-soup.txt';

let alphabetHashMap = {};
let wordList = [];

let lineCount = 0;
let gridWidth = 0;
let gridHeight = 0;
let gridSize;
const processLine = (line) => {
  // First line indicates puzzle grid size
  if (lineCount === 0) {
    gridSize = parseGridsize(line);
  }
  gridWidth = gridSize.gridWidth;
  gridHeight = gridSize.gridHeight;
  // console.log('@!@! gridLength, gridHeight, gridSize', gridWidth, gridHeight, gridSize);

  if (lineCount > 0 && lineCount <= gridHeight) {
    alphabetHashMap = fillAlphabetHashMap(line, lineCount, alphabetHashMap);
  }
  // console.log('@!@! alphabetHashMap', alphabetHashMap);

  if (lineCount > gridHeight) {
    wordList.push(line);
  }

  lineCount++;
  console.log(line);
};

const checkOneWord = (word) => {
  // console.log('@!@! checkOneWord ', word);
  const letters = word.split('');
  let notFound = false;
  const indices = [];
  const letterLocations = {};
  letters.forEach((letter) => {
    // console.log('@!@! letter, letterLocations ', letter, letterLocations);
    const llocs = alphabetHashMap[letter];
    if (!llocs || llocs.length === 0) {
      notFound = true;
    }
    letterLocations[letter] = llocs;
  });
  if (notFound) {
    return false;
  }
  console.log('@!@! letterLocations', letterLocations);
  // Start with each letter location and fan out from there.
  // for the first letter any direction is fine
  // but the second letter onward the direction is fixed.
  // it is much easier to show the negative i.e word not found
  // than it is to find the word. We can add a short circuit
  // if we need to also check the negative case.
  //   @!@! letter locations O {
  //   H: [ [ 1, 0 ], [ 2, 4 ] ],
  //   E: [ [ 2, 1 ] ],
  //   L: [ [ 3, 2 ], [ 4, 3 ] ],
  //   O: [ [ 5, 1 ], [ 5, 2 ], [ 5, 4 ] ]
  // }

  const foundLetters = Object.keys(letterLocations);
  const foundLocations = [];
  console.log('@!@! foundLetters', foundLetters);
  if (foundLetters && foundLetters.length) {
    let direction;
    letters.forEach((letter, letterIndex) => {
      console.log('@!@! letter+locations', letter, letterLocations[letter]);
      letterLocations[letter] &&
        letterLocations[letter].length &&
        letterLocations[letter].forEach((loc) => {
          const results = checkNextLetter(
            letter,
            loc,
            direction,
            letterLocations[foundLetters[letterIndex + 1]],
          );
          console.log('@!@! results ', results);
          if (results.found) {
            direction = results.direction;
            // first add the current letter's locations
            foundLocations.push(results.currentLocation);
            console.log(
              '@!@! found locations after checkNextLetter',
              foundLocations,
            );
          }
        });
    });
    console.log('@!@! found locations', foundLocations);
  }
};

const loadAndProcessFile = () => {
  lineReader.eachLine(
    myFile,
    function (line) {
      processLine(line);
      return;
    },
    function (err) {
      if (err) throw err;
      logger.write(
        `processing complete - ${JSON.stringify(alphabetHashMap)} \n`,
      );
      // console.log(
      //   `processing complete now checking words - ${JSON.stringify(
      //     alphabetHashMap,
      //   )} \n`,
      // );
      processWords();
    },
  );
};

const processWords = () => {
  console.log('@!@! processWords', wordList);
  wordList.forEach((word) => checkOneWord(word));
};

loadAndProcessFile();
