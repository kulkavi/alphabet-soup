const parseGridsize = (firstLine) => {
  const [gridWidth, gridHeight] = firstLine.split('x');
  console.log('@!@! in parseGridsize gridWidth, gridHeight', gridWidth, gridHeight);
  return { gridWidth, gridHeight };
};

module.exports = parseGridsize;

