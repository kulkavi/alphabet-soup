  const directionMap = {
    up: -1,
    left: -1,
    down: 1,
    right: 1,
  };

  const checkNextLetter = (letter, previousLocation, direction, possibleLocations) => {
    let found = false;
    let currentLocation = null;
    let foundDirection = direction || null;
    console.log(
      '@!@! checkNextLetter',
      letter,
      previousLocation,
      direction,
      possibleLocations,
    );
    possibleLocations &&
      possibleLocations.forEach((posLoc) => {
        // check each possibleLocation for a possible next letter
        console.log(
          '@!@! checkNextLetter previousLocation, posLoc',
          previousLocation,
          posLoc,
        );
        verticalDiff = posLoc[0] - previousLocation[0];
        horizontalDiff = posLoc[1] - previousLocation[1];
        console.log(
          '@!@! checkNextLetter verticalDiff, horizontalDiff',
          verticalDiff,
          horizontalDiff,
        );
        if (!direction) {
          let verticalDirection = '';
          let horizontalDirection = '';
          // since the direction is not set yet, check all directions for a possible next match
          if (Math.abs(verticalDiff) > 1 || Math.abs(horizontalDiff) > 1) {
            // not a possible match
            found = false;
          }
          if (verticalDiff === -1) {
            // same column so
            verticalDirection = 'up';
          }
          if (verticalDiff === 1) {
            // same column so
            verticalDirection = 'down';
          }
          if (horizontalDiff === 1) {
            // same column so
            horizontalDirection = 'right';
          }
          if (horizontalDiff === -1) {
            // same column so
            horizontalDirection = 'left';
          }
          foundDirection = `${verticalDirection}:${horizontalDirection}`;
          console.log(
            '@!@! checkNextLetter verticalDirection, horizontalDirection, foundDirection',
            verticalDirection,
            horizontalDirection,
            foundDirection,
          );
          found = true;
          currentLocation = posLoc;
        } else {
          // else only check in the direction needed
          const [x, y] = direction
            .split(':')
            .map((splitDir) => directionMap[splitDir]);
          if (
            previousLocation[0] + x === posLoc[0] &&
            previousLocation[1] + y === posLoc[1]
          ) {
            found = true;
            currentLocation = posLoc;
          }
          found = false;
        }
      });
    console.log(
      '@!@! checkNextLetter found, direction, foundDirection, possibleLocations',
      found,
      direction,
      foundDirection,
      currentLocation,
    );
    return { found, direction: foundDirection, currentLocation };
  };

module.exports = checkNextLetter;
